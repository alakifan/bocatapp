const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let bocateroSchema = new Schema({
    correo: String,
    password: String,
    bocateria: {type: Schema.ObjectId, ref: 'Bocateria'}
});

module.exports = mongoose.model('Bocatero', bocateroSchema);