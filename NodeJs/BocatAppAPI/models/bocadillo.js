const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let bocadilloSchema = new Schema({
    nombre: String,
    imagen: String,
    ingredientes: [String],
    precio: Number,
    descuento: Number,
    bocateria: {type: Schema.ObjectId, ref: 'Bocateria'},
    refresco: Boolean,
    extraQueso: Boolean,
    salsa: String
});

module.exports = mongoose.model('Bocadillo', bocadilloSchema);