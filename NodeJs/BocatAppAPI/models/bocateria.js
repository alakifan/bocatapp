const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let bocateriaSchema = new Schema({
    nombre: String,
    telefono: String,
    horario: [String],
    imagen: String,
    latitud: Number,
    longitud: Number,
    owner: {type: Schema.ObjectId, ref: 'Bocatero'},
    pedidos: [{type: Schema.ObjectId, ref: 'Pedido'}]
});

module.exports = mongoose.model('Bocateria', bocateriaSchema);