const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let pedidoSchema = new Schema({
    usuario: {type: Schema.ObjectId, ref: 'User'},
    total: Number,
    horaRecogida: String,
    bocadillos: [{type: Schema.ObjectId, ref: 'Bocadillo'}],
    bocateria: {type: Schema.ObjectId, ref: 'Bocateria'},
    completado: Boolean
});

module.exports = mongoose.model('Pedido', pedidoSchema);