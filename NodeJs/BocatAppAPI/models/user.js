const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//const bcrypt = require('bcrypt-nodejs');

let userSchema = new Schema({
    nombre: String,
    email: {type: String, unique: true},
    password: {type: String, select: false},
    bocadillosFav: [{type: Schema.ObjectId, ref: 'Bocadillo'}],
    pedidos: [{type: Schema.ObjectId, ref: 'Pedido'}]
});

module.exports = mongoose.model('User', userSchema);