const express = require('express');
const bocateriaController = require('../controllers/bocaterias');
let router = express.Router();

router.get('/findAll', bocateriaController.findAllBocaterias);
router.post('/findOne', bocateriaController.findOneBocateria);
router.post('/findBocadillos', bocateriaController.findBocatas);

module.exports = router;