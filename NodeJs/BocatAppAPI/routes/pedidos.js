const express = require('express');
const pedidosController = require('../controllers/pedidos');
let router = express.Router();

router.post('/hacerPedido', pedidosController.hacerPedido);
router.post('/find', pedidosController.pedidoId);
router.post('/getPedidos', pedidosController.getPedidos);
router.post('/setCompletado', pedidosController.setCompletado);

module.exports = router;