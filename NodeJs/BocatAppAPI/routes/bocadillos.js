const express = require('express');
const bocadilloController = require('../controllers/bocadillos');
let router = express.Router();

router.post('/addBocadillo', bocadilloController.addBocadillo);
router.put('/editBocadillo', bocadilloController.editBocadillo);
router.delete('/deleteBocadillo', bocadilloController.removeBocadillo);
router.put('/addToFav', bocadilloController.addToFavoritos);
router.put('/removeFromFav', bocadilloController.removeFavorito);
router.post('/getFavs', bocadilloController.getFavoritos);
router.post('/addBocadilloPedido', bocadilloController.addBocadilloPedido);

module.exports = router;