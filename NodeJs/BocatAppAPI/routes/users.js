const express = require('express');
const userController = require('../controllers/users');
const bocateroController = require('../controllers/bocateros');
let router = express.Router();

router.post('/register', userController.signUp);
router.post('/login', userController.signIn);
router.post('/loginBocatero', bocateroController.signIn);

module.exports = router;
