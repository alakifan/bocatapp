const service = require('../services');
const Bocatero = require('../models/bocatero');

module.exports.signIn = (req, res) => {
    Bocatero.findOne({correo: req.body.correo, password: req.body.password}).exec((err, user) => {
        Bocatero.populate(user, {path: 'bocateria'}, (err, user) => {
            if (err) return res.status(401).jsonp({error: 401, mensaje: 'Error en la autenticación'});
            if (!user) return res.status(404).jsonp({error: 404, mensaje: 'No existe el bocatero'});
            return res.status(201).jsonp({
                token: service.createToken(user),
                correo: user.correo,
                bocateria: user.bocateria,
                _id: user._id
            });
        });
    });
};