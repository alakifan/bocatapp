const service = require('../services');
const User = require('../models/user');

module.exports.signUp = (req, res) => {
    let user = new User({
        nombre: req.body.nombre,
        email: req.body.email,
        password: req.body.password,
        bocadillosFav: [],
        pedidos: []
    });
    user.save((err, result) => {
        if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.mensaje}`});
        return res.status(201).jsonp({
            token: service.createToken(user),
            nombre: result.nombre,
            email: result.email,
            _id: result._id
        });
    });
};
module.exports.signIn = (req, res) => {
    User.findOne({email: req.body.email, password: req.body.password}).exec((err, user) => {
        User.populate(user, {path: 'pedidos'}, (err, user) => {
            User.populate(user, {path: 'bocadillosFav'}, (err, user) => {
                if (err) return res.status(401).jsonp({error: 401, mensaje: 'Error en la autenticación'});
                if (!user) return res.status(404).jsonp({error: 404, mensaje: 'No existe el usuario'});
                return res.status(201).jsonp({
                    token: service.createToken(user),
                    nombre: user.nombre,
                    email: user.email,
                    _id: user._id
                });
            });
        });
    });
};