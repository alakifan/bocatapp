const mongoose = require('mongoose');
const Pedido = require('../models/pedido');
const Bocateria = require('../models/bocateria');
const Bocadillo = require('../models/bocadillo');
const Usuario = require('../models/user');

module.exports.hacerPedido = (req, res) => {
    let user_id = req.body._id;
    let bocateria_id = req.body.bocateria;
    Usuario.findById(user_id, (err, user) => {
        if (err) return res.status(401).jsonp({error: 401, mensaje: 'Error en la autenticación 1'});
        if (!user) return res.status(404).jsonp({error: 404, mensaje: 'No existe el usuario'});
        Bocadillo.find({_id: {$in: req.body.bocadillos}}, (err, bocadillos) => {
            if (err) return res.status(500).jsonp({error: 500, mensaje: `4 ${err.mensaje}`});
            Bocateria.findById(bocateria_id, (err, bocateria) => {
                if (err) return res.status(500).jsonp({error: 500, mensaje: `3 ${err.mensaje}`});
                if (!bocateria) return res.status(404).jsonp({error: 404, mensaje: 'No existe la bocateria'});
                let pedido = new Pedido({
                    usuario: user,
                    total: req.body.total,
                    horaRecogida: req.body.horaRecogida,
                    bocadillos: bocadillos,
                    bocateria: bocateria,
                    completado: req.body.completado
                });
                pedido.save();
                Usuario.findByIdAndUpdate(user_id, {$push: {pedidos: mongoose.Types.ObjectId(pedido._id)}}).exec((err, user) => {
                    if (err) return res.status(401).jsonp({error: 401, mensaje: `${err.message}`});
                    if (!user) return res.status(404).jsonp({error: 404, mensaje: 'No existe el usuario'});
                    Bocateria.findByIdAndUpdate(bocateria_id, {$push: {pedidos: mongoose.Types.ObjectId(pedido._id)}}).exec((err, bocateria) => {
                        if (err) return res.status(500).jsonp({error: 500, mensaje: `1 ${err.mensaje}`});
                        if (!bocateria) return res.status(404).jsonp({error: 404, mensaje: 'No existe la bocateria'});
                        return res.status(200).jsonp(pedido);
                    });
                });
            });
        });
    });
};
module.exports.pedidoId = (req, res) => {
    Pedido.findById(req.body._id, (err, pedido) => {
        Pedido.populate(pedido, {path: 'bocadillos'}, (err, pedido) => {
            Pedido.populate(pedido, {path: 'usuario'}, (err, pedido) => {
                if (err) return res.status(401).jsonp({error: 401, mensaje: `${err.message}`});
                if (!pedido) return res.status(404).jsonp({error: 404, mensaje: 'No existe el pedido'});
                return res.status(200).jsonp(pedido);
            });
        });
    });
};
module.exports.setCompletado = (req, res) => {
    Pedido.findOneAndUpdate({_id: req.body._id}, {$set: {completado: true}}, (err, pedido) => {
        if (err) return res.status(401).jsonp({error: 401, mensaje: `${err.message}`});
        if (!pedido) return res.status(404).jsonp({error: 404, mensaje: 'No existe el pedido'});
        return res.status(200).jsonp({mensaje: 'exito'});
    })
};
module.exports.getPedidos = (req, res) => {
    Usuario.findOne({_id: req.body._id}).exec((err, user) => {
        Usuario.populate(user, {path: 'pedidos', populate: {path: 'bocadillos'}}, (err, user) => {
            if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.message}`});
            if (!user) return res.status(404).jsonp({error: 404, mensaje: "No existe ese usuario"});
            return res.status(201).jsonp(user.pedidos);
        });
    });
};