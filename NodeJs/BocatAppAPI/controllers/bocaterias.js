const Bocateria = require('../models/bocateria');
const Bocadillo = require('../models/bocadillo');
const mongoose = require('mongoose');

module.exports.findAllBocaterias = (req, res) => {
    Bocateria.find({}, {}, (err, bocaterias) => {
        if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.mensaje}`});
        if (bocaterias && bocaterias.length){
            res.status(200).jsonp(bocaterias);
        }else{
            res.sendStatus(404);
        }
    })
};
module.exports.findOneBocateria = (req, res) => {
    Bocateria.findOne({telefono: req.body.telefono}).exec((err, bocateria) => {
        if (err) return res.status(401).jsonp({error: 401, mensaje: `${err.message}`});
        if (!bocateria){
            return res.status(404).jsonp({error: 404, mensaje: "Número incorrecto"});
        }else{
            res.status(200).jsonp(bocateria);
        }
    });
};
module.exports.findBocatas = (req, res) => {
    Bocateria.findOne({telefono: req.body.telefono}).exec((err, bocateria) => {
        if (err) return res.status(401).jsonp({error: 401, mensaje: `${err.message}`});
        if (!bocateria) return res.status(404).jsonp({error: 404, mensaje: "No existe esa bocatería"});
        Bocadillo.find({bocateria: mongoose.Types.ObjectId(bocateria._id)}, {}, (err, bocadillos) => {
            if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.mensaje}`});
            if (bocadillos && bocadillos.length){
                res.status(200).jsonp(bocadillos);
            }else{
                res.status(404);
            }
        });
    });
};
