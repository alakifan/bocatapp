const Bocateria = require('../models/bocateria');
const Bocadillo = require('../models/bocadillo');
const mongoose = require('mongoose');
const User = require('../models/user');
const imgur = require('imgur');
const config = require('../config');

module.exports.addBocadilloPedido = (req, res) => {
    let bocadillo = new Bocadillo({
        nombre: req.body.nombre,
        precio: req.body.precio,
        refresco: req.body.refresco,
        extraQueso: req.body.extraQueso,
        salsa: req.body.salsa
    });
    bocadillo.save((err, result) => {
        if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.message}`});
        return res.status(201).jsonp(result);
    });
};

module.exports.addBocadillo = (req, res) => {
    Bocateria.findOne({telefono: req.body.telefono}).exec((err, bocateria) => {
        if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.message}`});
        if (!bocateria) return res.status(404).jsonp({error: 404, mensaje: "no existe esa bocateria"});

        let img = req.files.imagen;

        let imgBase64 = img.data.toString('base64');

        imgur.setClientId(config.CLIENT_ID);

        imgur.uploadBase64(imgBase64).then(function (json) {

            let bocadillo = new Bocadillo({
                nombre: req.body.nombre,
                imagen: json.data.link,
                ingredientes: req.body.ingredientes,
                precio: req.body.precio,
                descuento: req.body.descuento,
                bocateria: mongoose.Types.ObjectId(bocateria._id),
                refresco: req.body.refresco,
                extraQueso: req.body.extraQueso,
                salsa: req.body.salsa
            });
            bocadillo.save((err, result) => {
                if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.message}`});
                return res.status(201).jsonp({
                    nombre: result.nombre,
                    precio: result.precio,
                    bocateria: result.bocateria
                });
            });
        }).catch(function (err) {
            console.error(err.message);
        });
    });
};
module.exports.editBocadillo = (req, res) => {
    Bocadillo.findOneAndUpdate({_id: req.body._id},
        {$set:{nombre: req.body.nombre, precio: req.body.precio, ingredientes: req.body.ingredientes, descuento: req.body.descuento}},
        (err, bocadillo) => {
            if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.message}`});
            return res.status(201).jsonp(bocadillo);
        });
};
module.exports.removeBocadillo = (req, res) => {
    Bocadillo.remove({_id: req.body._id}, err => {
        if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.message}`});
        return res.status(200).jsonp({mensaje: 'Borrado efectuado con éxito'});
    });
};
module.exports.addToFavoritos = (req, res) => {
    Bocadillo.findOne({_id: req.body._id}).exec((err, bocadillo) => {
        if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.message}`});
        if (!bocadillo) return res.status(404).jsonp({error: 404, mensaje: "No existe ese bocadillo"});
        User.findOneAndUpdate({email: req.body.email}, {$push: {bocadillosFav: bocadillo}}).exec((err, user) => {
            if (!user) return res.status(404).jsonp({error: 404, mensaje: "Usuario no encontrado"});
            User.populate(user, {path: "bocadillosFav"},(err, user) => {
                if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.message}`});
                if (!user) return res.status(404).jsonp({error: 404, mensaje: "Usuario no encontrado"});
                return res.status(201).jsonp(user);
            });
        });
    });
};
module.exports.removeFavorito = (req, res) => {
    Bocadillo.findOne({_id: req.body._id}).exec((err, bocadillo) => {
        if (err) return res.status(500).jsonp({error: 500, mensaje: `Bocadillo ${err.message}`});
        if (!bocadillo) return res.status(404).jsonp({error: 404, mensaje: "No existe ese bocadillo"});
        User.findOneAndUpdate({email: req.body.email}, {$pull: {bocadillosFav: mongoose.Types.ObjectId(bocadillo._id)}}).exec((err, user) => {
            if (err) return res.status(500).jsonp({error: 500, mensaje: `Usuario ${err.message}`});
            if (!user) return res.status(404).jsonp({error: 404, mensaje: "Usuario no encontrado"});
            return res.status(201).jsonp(user);
        });
    });
};
module.exports.getFavoritos = (req, res) => {
    User.findOne({_id: req.body._id}).exec((err, user) => {
        User.populate(user, {path: 'bocadillosFav'}, (err, user) => {
            if (err) return res.status(500).jsonp({error: 500, mensaje: `${err.message}`});
            if (!user) return res.status(404).jsonp({error: 404, mensaje: "No existe ese usuario"});
            return res.status(201).jsonp(user.bocadillosFav);
        });
    });
};