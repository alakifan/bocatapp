const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const auth = require('./middlewares/auth');
const mongoose = require('mongoose');
const logger =  require('morgan');
const config = require('./config');
const cors = require('cors');
const users = require('./routes/users');
const bocatas = require('./routes/bocadillos');
const bocaterias = require('./routes/bocaterias');
const pedidos = require('./routes/pedidos');
const fileUpload = require('express-fileupload');

mongoose.Promise = global.Promise;
mongoose.connect(config.MONGODB_URI).then(() => {

}, (err) => {

});

let app = express();

app.use(logger('dev'));
app.use(fileUpload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors({origin: '*', credentials: true}));
app.use('/api/v1/auth', users);
app.use('/api/v1/bocata', auth.isAuth, bocatas);
app.use('/api/v1/bocateria', auth.isAuth, bocaterias);
app.use('/api/v1/pedidos', auth.isAuth, pedidos);

module.exports = app;
