import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../servicios/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  correo = '';
  password = '';

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.authService.checkLoggedIn();
  }
  doLogin() {
    this.authService.doLogin(this.correo, this.password).subscribe(bocatero => {
      this.authService.setLoginData(bocatero.token, bocatero.bocateria);

      this.router.navigate(['/pedidoList']);
    }, error => {
      alert('Login incorrecto');
    });
  }

}
