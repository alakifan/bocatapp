import { Component, OnInit } from '@angular/core';
import {Bocadillo} from '../../models/Bocadillo';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {PedidosService} from '../../servicios/pedidos.service';
import {AuthService} from '../../servicios/auth.service';
import {BocadillosService} from '../../servicios/bocadillos.service';

@Component({
  selector: 'app-lista-bocadillos',
  templateUrl: './lista-bocadillos.component.html',
  styleUrls: ['./lista-bocadillos.component.css']
})
export class ListaBocadillosComponent implements OnInit {

  myBocadillos: Array<Bocadillo> = [];
  modalRef: NgbModalRef;
  bocadillo: Bocadillo;
  nombre = '';
  precio: string;
  ingredientes: Array<string> = [];
  ingredientesNuevo: Array<string> = [];
  descuento: string;
  imagenBocadillo: FileList;
  imagenBocadilloActual: File;

  constructor(private pedidosService: PedidosService, private authService: AuthService,
              private modalService: NgbModal, private router: Router, private bocadillosService: BocadillosService) { }

  ngOnInit() {
    this.authService.checkAuth();
    this.bocadillosService.getBocadillos().subscribe(result => {
      this.myBocadillos = result;
    });
  }
  selectFile(event) {
    this.imagenBocadillo = event.target.files;
  }
  cerrarSesion() {
    this.authService.doLogout();
    this.router.navigate(['/']);
  }
  goToPedidos() {
    this.router.navigate(['/pedidoList']);
  }
  abrirModal(modalToOpen: any) {
    this.modalRef = this.modalService.open(modalToOpen);
  }
  getBocadillo(bocadillo: Bocadillo) {
    this.bocadillo = bocadillo;
  }
  deleteBocadillo(_id: string) {
    this.bocadillosService.deleteBocadillo(_id).subscribe(result => {
      window.location.reload();
    }, error => {
      alert('Error en el borrado');
    });
  }
  confirmarBorrado(_id: string) {
    const confirm = window.confirm('¿Seguro que quieres borrarlo?');
    if (confirm) {
      this.deleteBocadillo(_id);
    }
  }
  editarBocadillo(_id: string) {
    this.bocadillosService.editBocadillo(_id, this.nombre, this.precio, this.ingredientes, '0').subscribe(result => {
      window.location.reload();
    }, error => {
      alert(error.toString());
    });
  }
  addBocadillo() {
    this.imagenBocadilloActual = this.imagenBocadillo.item(0);
    this.bocadillosService.addBocadillo(this.imagenBocadilloActual, this.nombre, this.ingredientesNuevo, this.precio).subscribe(result => {
      window.location.reload();
    }, error => {
      alert('Algo salió mal');
    });
    this.imagenBocadillo = undefined;
  }
}
