import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaBocadillosComponent } from './lista-bocadillos.component';

describe('ListaBocadillosComponent', () => {
  let component: ListaBocadillosComponent;
  let fixture: ComponentFixture<ListaBocadillosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaBocadillosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaBocadillosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
