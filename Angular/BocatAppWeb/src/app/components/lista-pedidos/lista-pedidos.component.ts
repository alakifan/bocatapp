import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../servicios/auth.service';
import {Bocateria} from '../../models/Bocateria';
import {Pedido} from '../../models/Pedido';
import {PedidosService} from '../../servicios/pedidos.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-lista-pedidos',
  templateUrl: './lista-pedidos.component.html',
  styleUrls: ['./lista-pedidos.component.css']
})
export class ListaPedidosComponent implements OnInit {

  bocateria: Bocateria;
  pedidos: Array<Pedido> = [];

  constructor(private pedidosService: PedidosService, private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.authService.checkAuth();
    this.pedidosService.getBocateria().subscribe(result => {
      this.bocateria = new Bocateria(result._id, result.horario, result.pedidos, result.nombre, result.telefono, result.imagen,
        result.latitud, result.longitud, result.owner);
      for (const pedidoId of this.bocateria.pedidos) {
        this.pedidosService.getPedido(pedidoId).subscribe(pedido => {
          if (!pedido.completado) {
            this.pedidos.push(pedido);
          }
        }, error => {
          alert(error.toString());
        });
      }
    });
  }
  cerrarSesion() {
    this.authService.doLogout();
    this.router.navigate(['/']);
  }
  setCompletado(_id: string) {
    this.pedidosService.setCompletado(_id).subscribe(result => {
      const card = document.getElementById(_id);
      card.classList.add('bg-success');
      card.classList.add('text-white');
    }, error => {
      alert('Error en el servidor');
    });
  }
  goToBocadillos() {
    this.router.navigate(['/bocadilloList']);
  }
}
