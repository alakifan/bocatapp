import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PedidosService {

  pedidoUrl = 'https://bocatappapi-pizbibldmm.now.sh/api/v1';

  constructor(private http: HttpClient, private authService: AuthService) { }
  requestOptions = {
    headers: new HttpHeaders({
      'Authorization': `Bearer ${this.authService.getToken()}`,
      'Content-Type': 'application/json'
    })
  };
  getBocateria(): Observable<any> {
    return this.http.post<any>(`${this.pedidoUrl}/bocateria/findOne`,
      {'telefono': `${this.authService.getBocateriaTlf()}`}, this.requestOptions);
  }
  getPedido(_id: string): Observable<any> {
    return this.http.post<any>(`${this.pedidoUrl}/pedidos/find`, {'_id': `${_id}`}, this.requestOptions);
  }
  setCompletado(_id: string): Observable<any> {
    return this.http.post<any>(`${this.pedidoUrl}/pedidos/setCompletado`, {'_id': `${_id}`}, this.requestOptions);
  }
}
