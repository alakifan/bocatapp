import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class BocadillosService {

  pedidoUrl = 'https://bocatappapi-pizbibldmm.now.sh/api/v1';

  constructor(private http: HttpClient, private authService: AuthService) { }
  requestOptions = {
    headers: new HttpHeaders({
      'Authorization': `Bearer ${this.authService.getToken()}`,
      'Content-Type': 'application/json'
    })
  };
  getBocadillos(): Observable<any> {
    return this.http.post<any>(`${this.pedidoUrl}/bocateria/findBocadillos`,
      {'telefono': `${this.authService.getBocateriaTlf()}`}, this.requestOptions);
  }
  deleteBocadillo(_id: string): Observable<any> {
    return this.http.request('delete', `${this.pedidoUrl}/bocata/deleteBocadillo`,
      {body: {'_id': _id}, headers: {'Authorization': `Bearer ${this.authService.getToken()}`, 'Content-Type': 'application/json'}});
  }
  editBocadillo(_id: string, nombre: string, precio: string, ingredientes: Array<string>, descuento: string): Observable<any> {
    return this.http.put<any>(`${this.pedidoUrl}/bocata/editBocadillo`,
      {'_id': _id, 'nombre': nombre, 'precio': Number(precio), 'ingredientes': ingredientes, 'descuento': Number(descuento)},
      this.requestOptions);
  }
  addBocadillo(imagen: File, nombre: string, ingredientes: Array<string>, precio: string): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('telefono', this.authService.getBocateriaTlf());
    formData.append('imagen', imagen);
    formData.append('nombre', nombre);
    formData.append('ingredientes', ingredientes[0]);
    formData.append('precio', precio);
    formData.append('descuento', '0');
    formData.append('refresco', 'false');
    formData.append('extraQueso', 'false');
    formData.append('salsa', 'ninguna');

    return this.http.request('post', `${this.pedidoUrl}/bocata/addBocadillo`,
      {body: formData, headers: {'Authorization': `Bearer ${this.authService.getToken()}`}});
  }
}
