import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Bocatero} from '../models/Bocatero';
import {Bocateria} from '../models/Bocateria';

const requestOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class AuthService {
  authUrl = 'https://bocatappapi-pizbibldmm.now.sh/api/v1';

  constructor(private http: HttpClient, private router: Router) { }

  doLogin (correo: string, password: string): Observable<Bocatero> {
    return this.http.post<Bocatero>(`${this.authUrl}/auth/loginBocatero`, {'correo': `${correo}`,
      'password': `${password}`}, requestOptions);
  }
  setLoginData(token: string, bocateria: Bocateria) {
    localStorage.setItem('token', token);
    localStorage.setItem('telefono', bocateria.telefono);
  }
  getToken(): string {
    return localStorage.getItem('token');
  }
  getBocateriaTlf(): string {
    return localStorage.getItem('telefono');
  }
  checkAuth() {
    const apiToken = localStorage.getItem('token');
    if (apiToken == null) {
      this.router.navigate(['/login']);
    }
  }
  checkLoggedIn() {
    const apiToken = localStorage.getItem('token');
    if (apiToken != null) {
      this.router.navigate(['/pedidoList']);
    }
  }
  doLogout() {
    localStorage.removeItem('token');
    localStorage.removeItem('telefono');
  }

}
