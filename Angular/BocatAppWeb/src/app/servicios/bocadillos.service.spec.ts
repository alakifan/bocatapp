import { TestBed, inject } from '@angular/core/testing';

import { BocadillosService } from './bocadillos.service';

describe('BocadillosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BocadillosService]
    });
  });

  it('should be created', inject([BocadillosService], (service: BocadillosService) => {
    expect(service).toBeTruthy();
  }));
});
