import {Bocadillo} from './Bocadillo';
import {Usuario} from './Usuario';


export class Pedido {
  public _id: string;
  public _bocadillos: Array<Bocadillo>;
  public _usuario: Usuario;
  public _total: number;
  public _horaRecogida: string;
  public _bocateria: string;
  public _completado: boolean;

  constructor(id: string, bocadillos: Array<Bocadillo>, usuario: Usuario, total: number, horaRecogida: string,
              bocateria: string, completado: boolean) {
    this._id = id;
    this._bocadillos = bocadillos;
    this._usuario = usuario;
    this._total = total;
    this._horaRecogida = horaRecogida;
    this._bocateria = bocateria;
    this._completado = completado;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get bocadillos(): Array<Bocadillo> {
    return this._bocadillos;
  }

  set bocadillos(value: Array<Bocadillo>) {
    this._bocadillos = value;
  }

  get usuario(): Usuario {
    return this._usuario;
  }

  set usuario(value: Usuario) {
    this._usuario = value;
  }

  get total(): number {
    return this._total;
  }

  set total(value: number) {
    this._total = value;
  }

  get horaRecogida(): string {
    return this._horaRecogida;
  }

  set horaRecogida(value: string) {
    this._horaRecogida = value;
  }

  get bocateria(): string {
    return this._bocateria;
  }

  set bocateria(value: string) {
    this._bocateria = value;
  }

  get completado(): boolean {
    return this._completado;
  }

  set completado(value: boolean) {
    this._completado = value;
  }
}
