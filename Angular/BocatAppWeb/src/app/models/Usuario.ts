export class Usuario {
  public _id: string;
  public _nombre: string;
  public _email: string;
  public _bocadillosFav: Array<string>;
  public _pedidos: Array<string>;

  constructor(id: string, nombre: string, email: string, bocadillosFav: Array<string>, pedidos: Array<string>) {
    this._id = id;
    this._nombre = nombre;
    this._email = email;
    this._bocadillosFav = bocadillosFav;
    this._pedidos = pedidos;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get nombre(): string {
    return this._nombre;
  }

  set nombre(value: string) {
    this._nombre = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get bocadillosFav(): Array<string> {
    return this._bocadillosFav;
  }

  set bocadillosFav(value: Array<string>) {
    this._bocadillosFav = value;
  }

  get pedidos(): Array<string> {
    return this._pedidos;
  }

  set pedidos(value: Array<string>) {
    this._pedidos = value;
  }
}
