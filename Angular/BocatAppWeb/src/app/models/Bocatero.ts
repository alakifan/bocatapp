import {Bocateria} from './Bocateria';


export class Bocatero {
  public _id: string;
  public _token: string;
  public _correo: string;
  public _password: string;
  public _bocateria: Bocateria;

  constructor(id: string, token: string, correo: string, password: string, bocateria: Bocateria) {
    this._id = id;
    this._token = token;
    this._correo = correo;
    this._password = password;
    this._bocateria = bocateria;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get token(): string {
    return this._token;
  }

  set token(value: string) {
    this._token = value;
  }

  get correo(): string {
    return this._correo;
  }

  set correo(value: string) {
    this._correo = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get bocateria(): Bocateria {
    return this._bocateria;
  }

  set bocateria(value: Bocateria) {
    this._bocateria = value;
  }
}
