

export class Bocadillo {
  public _id: string;
  public _ingredientes: Array<string>;
  public _nombre: string;
  public _imagen: string;
  public _precio: number;
  public _descuento: number;
  public _bocateria: string;
  public _refresco: boolean;
  public _extraQueso: boolean;
  public _salsa: string;

  constructor(id: string, ingredientes: Array<string>, nombre: string, imagen: string, precio: number,
              descuento: number, bocateria: string, refresco: boolean, extraQueso: boolean, salsa: string) {
    this._id = id;
    this._ingredientes = ingredientes;
    this._nombre = nombre;
    this._imagen = imagen;
    this._precio = precio;
    this._descuento = descuento;
    this._bocateria = bocateria;
    this._refresco = refresco;
    this._extraQueso = extraQueso;
    this._salsa = salsa;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get ingredientes(): Array<string> {
    return this._ingredientes;
  }

  set ingredientes(value: Array<string>) {
    this._ingredientes = value;
  }

  get nombre(): string {
    return this._nombre;
  }

  set nombre(value: string) {
    this._nombre = value;
  }

  get imagen(): string {
    return this._imagen;
  }

  set imagen(value: string) {
    this._imagen = value;
  }

  get precio(): number {
    return this._precio;
  }

  set precio(value: number) {
    this._precio = value;
  }

  get descuento(): number {
    return this._descuento;
  }

  set descuento(value: number) {
    this._descuento = value;
  }

  get bocateria(): string {
    return this._bocateria;
  }

  set bocateria(value: string) {
    this._bocateria = value;
  }

  get refresco(): boolean {
    return this._refresco;
  }

  set refresco(value: boolean) {
    this._refresco = value;
  }

  get extraQueso(): boolean {
    return this._extraQueso;
  }

  set extraQueso(value: boolean) {
    this._extraQueso = value;
  }

  get salsa(): string {
    return this._salsa;
  }

  set salsa(value: string) {
    this._salsa = value;
  }
}
