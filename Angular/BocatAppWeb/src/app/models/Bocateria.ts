

export class Bocateria {
  public _id: string;
  public _horario: Array<string>;
  public _pedidos: Array<string>;
  public _nombre: string;
  public _telefono: string;
  public _imagen: string;
  public _latitud: number;
  public _longitud: number;
  public _owner: string;


  constructor(id: string, horario: Array<string>, pedidos: Array<string>, nombre: string, telefono: string,
              imagen: string, latitud: number, longitud: number, owner: string) {
    this._id = id;
    this._horario = horario;
    this._pedidos = pedidos;
    this._nombre = nombre;
    this._telefono = telefono;
    this._imagen = imagen;
    this._latitud = latitud;
    this._longitud = longitud;
    this._owner = owner;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get horario(): Array<string> {
    return this._horario;
  }

  set horario(value: Array<string>) {
    this._horario = value;
  }

  get pedidos(): Array<string> {
    return this._pedidos;
  }

  set pedidos(value: Array<string>) {
    this._pedidos = value;
  }

  get nombre(): string {
    return this._nombre;
  }

  set nombre(value: string) {
    this._nombre = value;
  }

  get telefono(): string {
    return this._telefono;
  }

  set telefono(value: string) {
    this._telefono = value;
  }

  get imagen(): string {
    return this._imagen;
  }

  set imagen(value: string) {
    this._imagen = value;
  }

  get latitud(): number {
    return this._latitud;
  }

  set latitud(value: number) {
    this._latitud = value;
  }

  get longitud(): number {
    return this._longitud;
  }

  set longitud(value: number) {
    this._longitud = value;
  }

  get owner(): string {
    return this._owner;
  }

  set owner(value: string) {
    this._owner = value;
  }
}
