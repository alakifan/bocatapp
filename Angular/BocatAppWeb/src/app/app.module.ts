import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {Route, RouterModule} from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import {AuthService} from './servicios/auth.service';
import { ListaPedidosComponent } from './components/lista-pedidos/lista-pedidos.component';
import {PedidosService} from './servicios/pedidos.service';
import { ListaBocadillosComponent } from './components/lista-bocadillos/lista-bocadillos.component';
import {BocadillosService} from './servicios/bocadillos.service';

const routes: Route[] = [
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent},
  {path: 'pedidoList', component: ListaPedidosComponent},
  {path: 'bocadilloList', component: ListaBocadillosComponent},
  {path: '**', component: LoginComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListaPedidosComponent,
    ListaBocadillosComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot()
  ],
  providers: [AuthService, PedidosService, BocadillosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
