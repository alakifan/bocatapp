package com.averano.bocatapp.models;

import org.parceler.Parcel;

import java.util.Arrays;
import java.util.List;

/**
 * Created by alaki on 11/03/2018.
 */

@Parcel
public class Bocateria {

    private String _id;
    private String nombre;
    private String telefono;
    private String[] horario;
    private String imagen;
    private List<Bocadillo> bocadillos;
    private double latitud;
    private double longitud;

    public Bocateria(String _id) {
        this._id = _id;
    }

    public Bocateria(String _id, String nombre, String telefono, String[] horario, String imagen, List<Bocadillo> bocadillos, double latitud, double longitud) {
        this._id = _id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.horario = horario;
        this.imagen = imagen;
        this.bocadillos = bocadillos;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public Bocateria() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String[] getHorario() {
        return horario;
    }

    public void setHorario(String[] horario) {
        this.horario = horario;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public List<Bocadillo> getBocadillos() {
        return bocadillos;
    }

    public void setBocadillos(List<Bocadillo> bocadillos) {
        this.bocadillos = bocadillos;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bocateria bocateria = (Bocateria) o;

        if (Double.compare(bocateria.getLatitud(), getLatitud()) != 0) return false;
        if (Double.compare(bocateria.getLongitud(), getLongitud()) != 0) return false;
        if (getNombre() != null ? !getNombre().equals(bocateria.getNombre()) : bocateria.getNombre() != null)
            return false;
        if (getTelefono() != null ? !getTelefono().equals(bocateria.getTelefono()) : bocateria.getTelefono() != null)
            return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getHorario(), bocateria.getHorario())) return false;
        if (getImagen() != null ? !getImagen().equals(bocateria.getImagen()) : bocateria.getImagen() != null)
            return false;
        return getBocadillos() != null ? getBocadillos().equals(bocateria.getBocadillos()) : bocateria.getBocadillos() == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getNombre() != null ? getNombre().hashCode() : 0;
        result = 31 * result + (getTelefono() != null ? getTelefono().hashCode() : 0);
        result = 31 * result + Arrays.hashCode(getHorario());
        result = 31 * result + (getImagen() != null ? getImagen().hashCode() : 0);
        result = 31 * result + (getBocadillos() != null ? getBocadillos().hashCode() : 0);
        temp = Double.doubleToLongBits(getLatitud());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLongitud());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Bocateria{" +
                "_id='" + _id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", telefono='" + telefono + '\'' +
                ", horario=" + Arrays.toString(horario) +
                ", imagen='" + imagen + '\'' +
                ", bocadillos=" + bocadillos +
                ", latitud=" + latitud +
                ", longitud=" + longitud +
                '}';
    }
}
