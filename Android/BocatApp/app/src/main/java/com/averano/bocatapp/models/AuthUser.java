package com.averano.bocatapp.models;

import org.parceler.Parcel;

@Parcel
public class AuthUser {

    private String token;
    private String nombre;
    private String email;
    private String _id;

    public AuthUser(String token, String nombre, String email, String _id) {
        this.token = token;
        this.nombre = nombre;
        this.email = email;
        this._id = _id;
    }

    public AuthUser(String nombre, String email, String _id) {
        this.nombre = nombre;
        this.email = email;
        this._id = _id;
    }

    public AuthUser() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthUser authUser = (AuthUser) o;

        if (token != null ? !token.equals(authUser.token) : authUser.token != null) return false;
        if (nombre != null ? !nombre.equals(authUser.nombre) : authUser.nombre != null)
            return false;
        if (email != null ? !email.equals(authUser.email) : authUser.email != null) return false;
        return _id != null ? _id.equals(authUser._id) : authUser._id == null;
    }

    @Override
    public int hashCode() {
        int result = token != null ? token.hashCode() : 0;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (_id != null ? _id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AuthUser{" +
                "token='" + token + '\'' +
                ", nombre='" + nombre + '\'' +
                ", email='" + email + '\'' +
                ", _id='" + _id + '\'' +
                '}';
    }
}