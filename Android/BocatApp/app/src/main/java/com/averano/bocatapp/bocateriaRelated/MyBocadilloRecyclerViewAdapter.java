package com.averano.bocatapp.bocateriaRelated;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.pedidoRelated.PedidoFormFragment;
import com.averano.bocatapp.R;
import com.averano.bocatapp.models.Bocadillo;
import com.averano.bocatapp.services.BocatAppAPI;
import com.averano.bocatapp.services.ServiceGenerator;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyBocadilloRecyclerViewAdapter extends RecyclerView.Adapter<MyBocadilloRecyclerViewAdapter.ViewHolder> {

    private final List<Bocadillo> mValues;
    private Context ctx;
    private AuthUser user;
    private List<Bocadillo> bocadillosFav;
    private ProgressDialog nDialog;

    public MyBocadilloRecyclerViewAdapter(Context context, List<Bocadillo> items, AuthUser user, List<Bocadillo> bocadillosFav) {
        mValues = items;
        ctx = context;
        this.user = user;
        this.bocadillosFav = bocadillosFav;

        nDialog = new ProgressDialog(ctx);
        nDialog.setMessage(ctx.getString(R.string.cargando));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_bocadillo, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);

        nDialog.show();
        Picasso.with(ctx).load(mValues.get(position).getImagen()).resize(250, 250).into(holder.imagen);
        holder.nombre.setText(mValues.get(position).getNombre());
        holder.precio.setText(String.valueOf(mValues.get(position).getPrecio()) + "€");

        if (!bocadillosFav.isEmpty()){
            for(int i = 0; i < bocadillosFav.size(); i++){
                if (bocadillosFav.get(i).get_id().equals(holder.mItem.get_id())){
                    holder.fav.setImageResource(R.drawable.ic_star_black_24dp);
                    holder.fav.setTag("llena");
                    break;
                }
            }
        }
        nDialog.dismiss();
        holder.fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.fav.getTag() == null){
                    nDialog.show();
                    BocatAppAPI api = ServiceGenerator.createService(BocatAppAPI.class);

                    Call<AuthUser> call = api.addToFav("Bearer " + user.getToken(), holder.mItem.get_id(),
                            user.getEmail());

                    call.enqueue(new Callback<AuthUser>() {
                        @Override
                        public void onResponse(Call<AuthUser> call, Response<AuthUser> response) {
                            if (response.isSuccessful()){
                                Toast.makeText(ctx, R.string.added_to_fav, Toast.LENGTH_SHORT).show();
                                holder.fav.setImageResource(R.drawable.ic_star_black_24dp);
                                holder.fav.setTag("llena");
                                nDialog.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<AuthUser> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }else{
                    nDialog.show();
                    BocatAppAPI api = ServiceGenerator.createService(BocatAppAPI.class);

                    Call<AuthUser> call = api.removeFromFav("Bearer " + user.getToken(), holder.mItem.get_id(),
                            user.getEmail());

                    call.enqueue(new Callback<AuthUser>() {
                        @Override
                        public void onResponse(Call<AuthUser> call, Response<AuthUser> response) {
                            if (response.isSuccessful()){
                                Toast.makeText(ctx, R.string.eliminado_favs, Toast.LENGTH_SHORT).show();
                                holder.fav.setImageResource(R.drawable.ic_star_border_black_24dp);
                                holder.fav.setTag(null);
                                nDialog.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<AuthUser> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
            }
        });
        holder.pedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialogFragment = new PedidoFormFragment(holder.mItem);
                dialogFragment.show(((FragmentActivity)ctx).getSupportFragmentManager(), "PedidoFormFragment");
            }
        });
        holder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(ctx);
                builder.setTitle(R.string.ingredientes);
                String ingredientes = "";
                for (int i = 0; i < holder.mItem.getIngredientes().length; i++)
                    ingredientes = ingredientes.concat(holder.mItem.getIngredientes()[i] + "\n");
                builder.setMessage(ingredientes);
                builder.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mValues == null){
            return 0;
        }
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView imagen;
        public final TextView nombre;
        public final TextView precio;
        public final ImageView fav;
        public final Button pedir;
        public final ImageView info;
        public Bocadillo mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            imagen = view.findViewById(R.id.imagenBocadillo);
            nombre = view.findViewById(R.id.nombreBocadillo);
            precio = view.findViewById(R.id.precioBocadillo);
            fav = view.findViewById(R.id.favorito);
            pedir = view.findViewById(R.id.botonPedir);
            info = view.findViewById(R.id.boton_info);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }
}
