package com.averano.bocatapp.pedidoRelated;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.averano.bocatapp.R;
import com.averano.bocatapp.models.Bocadillo;
import com.averano.bocatapp.services.FormListener;

import java.util.ArrayList;
import java.util.List;

public class PedidoFormFragment extends DialogFragment {

    FormListener mListener;
    String token;
    Spinner salsas;
    CheckBox refresco, extraQueso;
    Bocadillo bocadillo;
    private List<String> salsaList = new ArrayList<>();

    public PedidoFormFragment() {

    }
    @SuppressLint("ValidFragment")
    public PedidoFormFragment(Bocadillo bocadillo) {
        this.bocadillo = bocadillo;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        salsaList.add(getContext().getString(R.string.ninguna));
        salsaList.add("mojo picón");
        salsaList.add("ali oli");
        salsaList.add(getContext().getString(R.string.mayonesa));
        salsaList.add("ketchup");

        View v = getActivity().getLayoutInflater().inflate(R.layout.fragment_pedido_form, null);

        salsas = v.findViewById(R.id.spinnerSalsa);
        refresco = v.findViewById(R.id.checkBoxRefresco);
        extraQueso = v.findViewById(R.id.checkBoxQueso);

        salsas.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, salsaList));

        salsas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                salsas.setSelection(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.como_quieres_bocadillo)
                .setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getContext(), R.string.added_carrito, Toast.LENGTH_SHORT).show();
                        bocadillo.setSalsa(salsas.getSelectedItem().toString());
                        bocadillo.setRefresco(refresco.isChecked());
                        bocadillo.setExtraQueso(extraQueso.isChecked());
                        bocadillo = mListener.onAceptarClick(bocadillo);

                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        builder.setView(v);
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (FormListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement FormListener");
        }
    }
}
