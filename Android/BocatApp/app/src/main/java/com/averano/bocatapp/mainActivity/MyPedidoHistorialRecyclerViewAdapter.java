package com.averano.bocatapp.mainActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.averano.bocatapp.R;
import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.models.Bocateria;
import com.averano.bocatapp.models.PedidoHist;
import com.averano.bocatapp.pedidoRelated.PedidoActivity;

import org.parceler.Parcels;

import java.util.List;

public class MyPedidoHistorialRecyclerViewAdapter extends RecyclerView.Adapter<MyPedidoHistorialRecyclerViewAdapter.ViewHolder> {

    private final List<PedidoHist> mValues;
    private Context ctx;
    private AuthUser user;

    public MyPedidoHistorialRecyclerViewAdapter(Context context, List<PedidoHist> items, AuthUser user) {
        ctx = context;
        mValues = items;
        this.user = user;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_pedido_historial, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        if (holder.bocadillos.getText().equals(""))
            for (int i = 0; i < holder.mItem.getBocadillos().size(); i++)
                holder.bocadillos.append(holder.mItem.getBocadillos().get(i).getNombre() + "\n");


        holder.pedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(ctx, PedidoActivity.class);

                Parcelable parcel = Parcels.wrap(user);
                Parcelable parcelPedido = Parcels.wrap(holder.mItem.getBocadillos());
                Parcelable parcelBocateria = Parcels.wrap(new Bocateria(holder.mItem.getBocteria()));

                i.putExtra("user", parcel);
                i.putExtra("bocadillosPedido", parcelPedido);
                i.putExtra("bocateria", parcelBocateria);

                ctx.startActivity(i);

            }
        });


    }

    @Override
    public int getItemCount() {
        if (mValues == null)
            return 0;
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView bocadillos;
        public final Button pedir;
        public PedidoHist mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            bocadillos = view.findViewById(R.id.historial_bocadillos);
            pedir = view.findViewById(R.id.historial_pedir);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + bocadillos.getText() + "'";
        }
    }
}
