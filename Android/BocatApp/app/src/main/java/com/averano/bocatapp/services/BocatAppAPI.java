package com.averano.bocatapp.services;

import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.models.Bocadillo;
import com.averano.bocatapp.models.Bocateria;
import com.averano.bocatapp.models.Pedido;
import com.averano.bocatapp.models.PedidoHist;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface BocatAppAPI {

    @FormUrlEncoded
    @POST("auth/login")
    Call<AuthUser> doLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("auth/register")
    Call<AuthUser> doRegister(
            @Field("nombre") String nombre,
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("bocateria/findAll")
    Call<List<Bocateria>> findAllBocaterias(
            @Header("Authorization") String token
    );

    @FormUrlEncoded
    @POST("bocateria/findOne")
    Call<Bocateria> findOneBocateria(
            @Header("Authorization") String token,
            @Field("telefono") String telefono
    );

    @FormUrlEncoded
    @POST("bocateria/findBocadillos")
    Call<List<Bocadillo>> findBocadillos(
            @Header("Authorization") String token,
            @Field("telefono") String telefono
    );

    @FormUrlEncoded
    @POST("bocata/addBocadilloPedido")
    Call<Bocadillo> addBocadilloPedido(
            @Header("Authorization") String token,
            @Field("nombre") String nombre,
            @Field("precio") Float precio,
            @Field("refresco") Boolean refresco,
            @Field("extraQueso") Boolean extraQueso,
            @Field("salsa") String salsa
    );

    @FormUrlEncoded
    @POST("pedidos/hacerPedido")
    Call<Pedido> hacerPedido(
            @Header("Authorization") String token,
            @Field("_id") String idUser,
            @Field("bocateria") String idBocateria,
            @Field("bocadillos") List<String> bocadillos,
            @Field("total") float total,
            @Field("horaRecogida") String horaRecogida,
            @Field("completado") boolean completado
    );

    @FormUrlEncoded
    @PUT("bocata/addToFav")
    Call<AuthUser> addToFav(
            @Header("Authorization") String token,
            @Field("_id") String idBocadillo,
            @Field("email") String email
    );

    @FormUrlEncoded
    @PUT("bocata/removeFromFav")
    Call<AuthUser> removeFromFav(
            @Header("Authorization") String token,
            @Field("_id") String idBocadillo,
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST("bocata/getFavs")
    Call<List<Bocadillo>> getFavs(
            @Header("Authorization") String token,
            @Field("_id") String idUsuario
    );

    @FormUrlEncoded
    @POST("pedidos/getPedidos")
    Call<List<PedidoHist>> getPedidos(
            @Header("Authorization") String token,
            @Field("_id") String idUser
    );

}
