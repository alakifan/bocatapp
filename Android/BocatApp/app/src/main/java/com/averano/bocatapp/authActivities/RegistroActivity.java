package com.averano.bocatapp.authActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.averano.bocatapp.R;
import com.averano.bocatapp.mainActivity.MainActivity;
import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.services.BocatAppAPI;
import com.averano.bocatapp.services.ServiceGenerator;

import org.parceler.Parcels;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroActivity extends FragmentActivity {

    Button registrarse;
    EditText nombre, correo, pass, repPass;
    ProgressDialog nDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getString(R.string.cargando));

        registrarse = findViewById(R.id.registrarse);
        nombre = findViewById(R.id.registroNombre);
        correo = findViewById(R.id.registroCorreo);
        pass = findViewById(R.id.registroPass);
        repPass = findViewById(R.id.registroRepePass);

        registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nombre.getText().toString().isEmpty() || correo.getText().toString().isEmpty() ||
                        pass.getText().toString().isEmpty() || repPass.getText().toString().isEmpty()){
                    Toast.makeText(RegistroActivity.this, R.string.rellena_todos, Toast.LENGTH_SHORT).show();
                }else if (!pass.getText().toString().equals(repPass.getText().toString())){
                    Toast.makeText(RegistroActivity.this, R.string.los_campos_coincidir, Toast.LENGTH_SHORT).show();
                }else{
                    nDialog.show();

                    BocatAppAPI api = ServiceGenerator.createService(BocatAppAPI.class);

                    Call<AuthUser> call = api.doRegister(
                            nombre.getText().toString(),
                            correo.getText().toString(),
                            pass.getText().toString()
                    );

                    call.enqueue(new Callback<AuthUser>() {
                        @Override
                        public void onResponse(Call<AuthUser> call, Response<AuthUser> response) {
                            AuthUser result = response.body();
                            if (response.isSuccessful()){
                                Intent i = new Intent(RegistroActivity.this, MainActivity.class);
                                Parcelable parcel = Parcels.wrap(result);
                                i.putExtra("user", parcel);
                                nDialog.dismiss();
                                startActivity(i);
                            }else{
                                Toast.makeText(RegistroActivity.this, R.string.error_registro, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<AuthUser> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
            }
        });
    }
}
