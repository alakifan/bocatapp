package com.averano.bocatapp.mainActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.averano.bocatapp.services.BocatAppAPI;
import com.averano.bocatapp.bocateriaRelated.BocateriaActivity;
import com.averano.bocatapp.R;
import com.averano.bocatapp.services.ServiceGenerator;
import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.models.Bocateria;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MapaFragment extends Fragment implements GoogleMap.OnInfoWindowClickListener{

    MapView mMapView;
    public GoogleMap mMap;
    AuthUser user;
    List<MarkerOptions> listaMarcadores = new ArrayList<>();
    ProgressDialog nDialog;

    public MapaFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mapa, container, false);

        nDialog = new ProgressDialog(getContext());
        nDialog.setMessage(getString(R.string.cargando));

        mMapView = rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        user = Parcels.unwrap(getArguments().getParcelable("user"));

        nDialog.show();
        BocatAppAPI api = ServiceGenerator.createService(BocatAppAPI.class);

        Call<List<Bocateria>> call = api.findAllBocaterias("Bearer " + user.getToken());

        call.enqueue(new Callback<List<Bocateria>>() {
            @Override
            public void onResponse(Call<List<Bocateria>> call, Response<List<Bocateria>> response) {
                if (response.isSuccessful()) {
                    List<Bocateria> listaBocaterias = response.body();
                    for (int i = 0; i < listaBocaterias.size(); i++){
                        String nombre, telefono;
                        Double lat, lon;

                        lat = listaBocaterias.get(i).getLatitud();
                        lon = listaBocaterias.get(i).getLongitud();

                        LatLng latLng = new LatLng(lat, lon);

                        nombre = listaBocaterias.get(i).getNombre();
                        telefono = listaBocaterias.get(i).getTelefono();

                        listaMarcadores.add(new MarkerOptions().position(latLng).title(nombre).snippet(telefono));
                    }
                    mMapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            mMap = googleMap;

                            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.map_style));

                            mMap.setOnInfoWindowClickListener(MapaFragment.this);

                            LatLng sevilla = new LatLng(37.3815398, -6.0062960);

                            for (int i = 0; i < listaMarcadores.size(); i++){
                                mMap.addMarker(listaMarcadores.get(i));
                            }

                            CameraPosition cameraPosition = new CameraPosition.Builder().target(sevilla).zoom(15).build();
                            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    });
                    nDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<Bocateria>> call, Throwable t) {
                t.printStackTrace();
            }
        });

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        return rootView;
    }


    @Override
    public void onInfoWindowClick(Marker marker) {

        nDialog.show();
        BocatAppAPI api = ServiceGenerator.createService(BocatAppAPI.class);

        Call<Bocateria> call = api.findOneBocateria("Bearer " + user.getToken(), marker.getSnippet());

        call.enqueue(new Callback<Bocateria>() {
            @Override
            public void onResponse(Call<Bocateria> call, Response<Bocateria> response) {
                Bocateria bocateria = response.body();
                if (response.isSuccessful()){
                    Intent i = new Intent(getContext(), BocateriaActivity.class);
                    Parcelable parcel = Parcels.wrap(bocateria);
                    Parcelable parcelUser = Parcels.wrap(user);
                    i.putExtra("bocateria", parcel);
                    i.putExtra("user", parcelUser);
                    nDialog.dismiss();
                    startActivity(i);
                }

            }
            @Override
            public void onFailure(Call<Bocateria> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
