package com.averano.bocatapp.mainActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.averano.bocatapp.R;
import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.models.Bocadillo;
import com.averano.bocatapp.models.Bocateria;
import com.averano.bocatapp.pedidoRelated.PedidoActivity;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class MyBocadillosFavRecyclerViewAdapter extends RecyclerView.Adapter<MyBocadillosFavRecyclerViewAdapter.ViewHolder> {

    private final List<Bocadillo> mValues;
    private Context ctx;
    private AuthUser user;
    ProgressDialog nDialog;


    public MyBocadillosFavRecyclerViewAdapter(Context context, List<Bocadillo> items, AuthUser user) {
        ctx = context;
        mValues = items;
        this.user = user;

        nDialog = new ProgressDialog(ctx);
        nDialog.setMessage(ctx.getString(R.string.cargando));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_bocadillos_fav, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.nombre.setText(mValues.get(position).getNombre());
        nDialog.show();
        Picasso.with(ctx).load(mValues.get(position).getImagen()).resize(200, 200).into(holder.imagen);
        nDialog.dismiss();
        holder.boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bocateria b = new Bocateria(holder.mItem.getBocateria());

                List<Bocadillo> bocadillo = new ArrayList<>();
                bocadillo.add(holder.mItem);

                Intent i = new Intent(ctx, PedidoActivity.class);

                Parcelable parcel = Parcels.wrap(user);
                Parcelable parcelBocateria = Parcels.wrap(b);
                Parcelable parcelBocadillo = Parcels.wrap(bocadillo);

                i.putExtra("user", parcel);
                i.putExtra("bocadillosPedido", parcelBocadillo);
                i.putExtra("bocateria", parcelBocateria);

                ctx.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mValues == null)
            return 0;
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nombre;
        public final ImageView imagen;
        public final Button boton;
        public Bocadillo mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nombre = view.findViewById(R.id.fav_nombreBocadillo);
            imagen = view.findViewById(R.id.fav_imagen);
            boton = view.findViewById(R.id.fav_pedidoRapido);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + nombre.getText() + "'";
        }
    }
}
