package com.averano.bocatapp.models;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class PedidoHist {

    private List<Bocadillo> bocadillos;
    private String _id;
    private float total;
    private String bocateria;
    private String horaRecogida;

    public PedidoHist(List<Bocadillo> bocadillos, String _id, float total, String bocateria, String horaRecogida) {
        this.bocadillos = bocadillos;
        this._id = _id;
        this.total = total;
        this.bocateria = bocateria;
        this.horaRecogida = horaRecogida;
    }

    public PedidoHist() {
    }

    public List<Bocadillo> getBocadillos() {
        return bocadillos;
    }

    public void setBocadillos(List<Bocadillo> bocadillos) {
        this.bocadillos = bocadillos;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getBocteria() {
        return bocateria;
    }

    public void setBocteria(String bocteria) {
        this.bocateria = bocteria;
    }

    public String getHoraRecogida() {
        return horaRecogida;
    }

    public void setHoraRecogida(String horaRecogida) {
        this.horaRecogida = horaRecogida;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PedidoHist that = (PedidoHist) o;

        if (Float.compare(that.getTotal(), getTotal()) != 0) return false;
        if (getBocadillos() != null ? !getBocadillos().equals(that.getBocadillos()) : that.getBocadillos() != null)
            return false;
        if (get_id() != null ? !get_id().equals(that.get_id()) : that.get_id() != null)
            return false;
        if (getBocteria() != null ? !getBocteria().equals(that.getBocteria()) : that.getBocteria() != null)
            return false;
        return getHoraRecogida() != null ? getHoraRecogida().equals(that.getHoraRecogida()) : that.getHoraRecogida() == null;
    }

    @Override
    public int hashCode() {
        int result = getBocadillos() != null ? getBocadillos().hashCode() : 0;
        result = 31 * result + (get_id() != null ? get_id().hashCode() : 0);
        result = 31 * result + (getTotal() != +0.0f ? Float.floatToIntBits(getTotal()) : 0);
        result = 31 * result + (getBocteria() != null ? getBocteria().hashCode() : 0);
        result = 31 * result + (getHoraRecogida() != null ? getHoraRecogida().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PedidoHist{" +
                "bocadillos=" + bocadillos +
                ", _id='" + _id + '\'' +
                ", total=" + total +
                ", bocateria='" + bocateria + '\'' +
                ", horaRecogida='" + horaRecogida + '\'' +
                '}';
    }
}
