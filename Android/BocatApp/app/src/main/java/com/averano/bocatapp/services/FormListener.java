package com.averano.bocatapp.services;

import com.averano.bocatapp.models.Bocadillo;

public interface FormListener {

    Bocadillo onAceptarClick(Bocadillo bocadillo);

}
