package com.averano.bocatapp.pedidoRelated;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.averano.bocatapp.R;
import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.models.Bocadillo;
import com.averano.bocatapp.models.Bocateria;
import com.averano.bocatapp.pedidoRelated.MyPedidoRecyclerViewAdapter;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class PedidoFragment extends Fragment {

    AuthUser user;
    Bocateria bocateria;
    private int mColumnCount = 1;
    List<Bocadillo> bocadilloList;
    MyPedidoRecyclerViewAdapter adapter;

    public PedidoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_pedido_list, container, false);

        bocadilloList = new ArrayList<>();

        user = Parcels.unwrap(getArguments().getParcelable("user"));
        bocateria = Parcels.unwrap(getArguments().getParcelable("bocateria"));
        bocadilloList = Parcels.unwrap(getArguments().getParcelable("bocadillosPedido"));

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyPedidoRecyclerViewAdapter(getContext(), bocadilloList));
        }
        return view;
    }


    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Bocadillo item);
    }
}
