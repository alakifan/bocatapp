package com.averano.bocatapp.models;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Pedido {

    private List<Bocadillo> bocadillos;
    private String _id;
    private float total;
    private Bocateria bocateria;

    public Pedido(List<Bocadillo> bocadillos, String _id, float total, Bocateria bocateria) {
        this.bocadillos = bocadillos;
        this._id = _id;
        this.total = total;
        this.bocateria = bocateria;
    }

    public Pedido(List<Bocadillo> bocadillos, String _id, float total) {
        this.bocadillos = bocadillos;
        this._id = _id;
        this.total = total;
    }

    public Pedido() {
    }

    public List<Bocadillo> getBocadillos() {
        return bocadillos;
    }

    public void setBocadillos(List<Bocadillo> bocadillos) {
        this.bocadillos = bocadillos;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Bocateria getBocateria() {
        return bocateria;
    }

    public void setBocateria(Bocateria bocateria) {
        this.bocateria = bocateria;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pedido pedido = (Pedido) o;

        if (Float.compare(pedido.getTotal(), getTotal()) != 0) return false;
        if (getBocadillos() != null ? !getBocadillos().equals(pedido.getBocadillos()) : pedido.getBocadillos() != null)
            return false;
        return get_id() != null ? get_id().equals(pedido.get_id()) : pedido.get_id() == null;
    }

    @Override
    public int hashCode() {
        int result = getBocadillos() != null ? getBocadillos().hashCode() : 0;
        result = 31 * result + (get_id() != null ? get_id().hashCode() : 0);
        result = 31 * result + (getTotal() != +0.0f ? Float.floatToIntBits(getTotal()) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "bocadillos=" + bocadillos +
                ", _id='" + _id + '\'' +
                ", total=" + total +
                ", bocateria=" + bocateria +
                '}';
    }
}
