package com.averano.bocatapp.pedidoRelated;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.averano.bocatapp.R;
import com.averano.bocatapp.bocateriaRelated.BocateriaActivity;
import com.averano.bocatapp.mainActivity.MainActivity;
import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.models.Bocadillo;
import com.averano.bocatapp.models.Bocateria;
import com.averano.bocatapp.models.Pedido;
import com.averano.bocatapp.services.BocatAppAPI;
import com.averano.bocatapp.services.PrecioChanger;
import com.averano.bocatapp.services.ServiceGenerator;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PedidoActivity extends FragmentActivity implements PedidoFragment.OnListFragmentInteractionListener,
        PrecioChanger{

    List<Bocadillo> bocadillosPedido = new ArrayList<>();
    AuthUser user;
    EditText horaRecogida;
    Bocateria bocateria;
    TextView totalPedido;
    float total = 0f;
    Button pedir;
    ProgressDialog nDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getString(R.string.cargando));

        totalPedido = findViewById(R.id.totalPedido);
        pedir = findViewById(R.id.botonPedir);
        horaRecogida = findViewById(R.id.horaRecogida);

        user = Parcels.unwrap(getIntent().getParcelableExtra("user"));
        bocadillosPedido = Parcels.unwrap(getIntent().getParcelableExtra("bocadillosPedido"));
        bocateria = Parcels.unwrap(getIntent().getParcelableExtra("bocateria"));

        if (bocadillosPedido != null)
            for (int i = 0; i < bocadillosPedido.size(); i++)
                total = total + bocadillosPedido.get(i).getPrecio();
        totalPedido.setText(String.format("%.2f", total) + "€");

        pedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!horaRecogida.getText().toString().contains(":"))
                    Toast.makeText(PedidoActivity.this, R.string.hora_valida, Toast.LENGTH_SHORT).show();
                else if (bocadillosPedido == null)
                    Toast.makeText(PedidoActivity.this, R.string.al_menos_un_bocadillo, Toast.LENGTH_SHORT).show();
                else{
                    List<String> idBocadilloList = new ArrayList<>();

                    for (int i = 0; i < bocadillosPedido.size(); i++)
                        idBocadilloList.add(bocadillosPedido.get(i).get_id());

                    nDialog.show();

                    BocatAppAPI api = ServiceGenerator.createService(BocatAppAPI.class);

                    Call<Pedido> call = api.hacerPedido("Bearer " + user.getToken(), user.get_id(),
                            bocateria.get_id(), idBocadilloList, total, horaRecogida.getText().toString(), false);

                    call.enqueue(new Callback<Pedido>() {
                        @Override
                        public void onResponse(Call<Pedido> call, Response<Pedido> response) {
                            if (response.isSuccessful()){
                                bocadillosPedido.clear();
                                Toast.makeText(PedidoActivity.this, R.string.pedido_exito, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(PedidoActivity.this, MainActivity.class);
                                Parcelable parcelUser = Parcels.wrap(user);
                                i.putExtra("user", parcelUser);
                                nDialog.dismiss();
                                startActivity(i);
                            }
                        }

                        @Override
                        public void onFailure(Call<Pedido> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
            }
        });

        Bundle bundle = new Bundle();
        Parcelable parcel = Parcels.wrap(user);
        Parcelable parcelBocadillos = Parcels.wrap(bocadillosPedido);
        Parcelable parcelBocateria = Parcels.wrap(bocateria);
        bundle.putParcelable("user", parcel);
        bundle.putParcelable("bocadillosPedido", parcelBocadillos);
        bundle.putParcelable("bocateria", parcelBocateria);
        PedidoFragment f = new PedidoFragment();
        f.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment2, f).commit();

    }

    @Override
    public void onListFragmentInteraction(Bocadillo item) {

    }

    @Override
    public void changePrecio(float precio) {

        total = total + precio;
        totalPedido.setText(String.format("%.2f", total) + "€");

    }
}
