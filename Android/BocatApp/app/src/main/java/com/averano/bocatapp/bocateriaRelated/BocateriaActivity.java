package com.averano.bocatapp.bocateriaRelated;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.SubtitleCollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.averano.bocatapp.pedidoRelated.PedidoActivity;
import com.averano.bocatapp.R;
import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.models.Bocadillo;
import com.averano.bocatapp.models.Bocateria;
import com.averano.bocatapp.services.BocatAppAPI;
import com.averano.bocatapp.services.FormListener;
import com.averano.bocatapp.services.ServiceGenerator;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BocateriaActivity extends AppCompatActivity implements BocadilloFragment.OnListFragmentInteractionListener, FormListener {

    ImageView fotoBocateria;
    List<Bocadillo> bocadillosPedido;
    AuthUser user;
    Bocateria bocateria;
    ProgressDialog nDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bocateria);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getString(R.string.cargando));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_DIAL);
                i.setData(Uri.parse("tel:" + bocateria.getTelefono()));
                if (i.resolveActivity(getPackageManager()) != null) {
                    startActivity(i);
                }
            }
        });

        bocadillosPedido = new ArrayList<>();

        user = Parcels.unwrap(getIntent().getParcelableExtra("user"));
        bocateria = Parcels.unwrap(getIntent().getParcelableExtra("bocateria"));

        Bundle bundle = new Bundle();
        Parcelable parcel = Parcels.wrap(user);
        Parcelable parcelBocateria = Parcels.wrap(bocateria);
        bundle.putParcelable("user", parcel);
        bundle.putParcelable("bocateria", parcelBocateria);
        BocadilloFragment f = new BocadilloFragment();
        f.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().add(R.id.layoutLista, f).commit();

        SubtitleCollapsingToolbarLayout toolbarLayout = findViewById(R.id.toolbar_layout);
        toolbarLayout.setTitle(bocateria.getNombre());
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String horario = "";
        switch (day) {
            case Calendar.MONDAY:
                horario = bocateria.getHorario()[0];
                break;
            case Calendar.TUESDAY:
                horario = bocateria.getHorario()[1];
                break;
            case Calendar.WEDNESDAY:
                horario = bocateria.getHorario()[2];
                break;
            case Calendar.THURSDAY:
                horario = bocateria.getHorario()[3];
                break;
            case Calendar.FRIDAY:
                horario = bocateria.getHorario()[4];
                break;
            case Calendar.SATURDAY:
                horario = bocateria.getHorario()[5];
                break;
            case Calendar.SUNDAY:
                horario = bocateria.getHorario()[6];
                break;
        }
        toolbarLayout.setSubtitle(horario);
        toolbarLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        fotoBocateria = findViewById(R.id.backdrop);
        nDialog.show();
        Picasso.with(this).load(bocateria.getImagen()).into(fotoBocateria);
        nDialog.dismiss();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bocateria, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.carrito) {
            Intent i = new Intent(this, PedidoActivity.class);
            Parcelable parcel = Parcels.wrap(bocadillosPedido);
            Parcelable parcelUser = Parcels.wrap(user);
            Parcelable parcelBocateria = Parcels.wrap(bocateria);
            i.putExtra("bocadillosPedido", parcel);
            i.putExtra("user", parcelUser);
            i.putExtra("bocateria", parcelBocateria);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(Bocadillo item) {

    }

    @Override
    public Bocadillo onAceptarClick(Bocadillo bocadillo) {
        Log.i("BOCADILLO", bocadillo.getSalsa() + String.valueOf(bocadillo.isRefresco()));

        nDialog.show();
        BocatAppAPI api = ServiceGenerator.createService(BocatAppAPI.class);

        Call<Bocadillo> call = api.addBocadilloPedido("Bearer " + user.getToken(), bocadillo.getNombre(),
                bocadillo.getPrecio(), bocadillo.isRefresco(), bocadillo.isExtraQueso(), bocadillo.getSalsa());

        call.enqueue(new Callback<Bocadillo>() {
            @Override
            public void onResponse(Call<Bocadillo> call, Response<Bocadillo> response) {
                Bocadillo b = new Bocadillo(response.body().get_id(), response.body().getNombre(),
                        response.body().getPrecio(), response.body().isRefresco(), response.body().isExtraQueso(),
                        response.body().getSalsa());
                nDialog.dismiss();
                bocadillosPedido.add(b);
            }

            @Override
            public void onFailure(Call<Bocadillo> call, Throwable t) {
                t.printStackTrace();
            }
        });

        return bocadillo;
    }
}
