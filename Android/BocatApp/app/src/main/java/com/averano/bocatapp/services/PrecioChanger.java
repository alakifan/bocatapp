package com.averano.bocatapp.services;

public interface PrecioChanger {

    void changePrecio (float precio);

}
