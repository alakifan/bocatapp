package com.averano.bocatapp.authActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.averano.bocatapp.R;
import com.averano.bocatapp.mainActivity.MainActivity;
import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.services.BocatAppAPI;
import com.averano.bocatapp.services.ServiceGenerator;

import org.parceler.Parcels;

public class LoginActivity extends FragmentActivity {

    TextView registro;
    Button entrar;
    EditText correo, pass;
    ProgressDialog nDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_login);

        nDialog = new ProgressDialog(this);
        nDialog.setMessage(getString(R.string.cargando));

        registro = findViewById(R.id.registro);
        entrar = findViewById(R.id.entrar);
        correo = findViewById(R.id.correo);
        pass = findViewById(R.id.pass);

        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });
        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (correo.getText().toString().isEmpty() || pass.getText().toString().isEmpty()){
                    Toast.makeText(LoginActivity.this, R.string.rellena_todos, Toast.LENGTH_SHORT).show();
                }else{
                    nDialog.show();
                    BocatAppAPI api = ServiceGenerator.createService(BocatAppAPI.class);

                    Call<AuthUser> call = api.doLogin(correo.getText().toString(), pass.getText().toString());

                    call.enqueue(new Callback<AuthUser>() {
                        @Override
                        public void onResponse(Call<AuthUser> call, Response<AuthUser> response) {
                            AuthUser result = response.body();
                            if (response.isSuccessful()){
                                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                Parcelable parcel = Parcels.wrap(result);
                                i.putExtra("user", parcel);
                                nDialog.dismiss();
                                startActivity(i);
                            }else{
                                Toast.makeText(LoginActivity.this, R.string.error_login, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<AuthUser> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
            }
        });
    }
}
