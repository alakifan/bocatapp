package com.averano.bocatapp.models;

import org.parceler.Parcel;

import java.util.Arrays;

@Parcel
public class Bocadillo {

    private String _id;
    private String nombre;
    private String imagen;
    private String[] ingredientes;
    private float precio;
    private float descuento;
    private String bocateria;
    private boolean refresco;
    private boolean extraQueso;
    private String salsa;

    public Bocadillo(String _id, String nombre, float precio, boolean refresco, boolean extraQueso, String salsa) {
        this._id = _id;
        this.nombre = nombre;
        this.precio = precio;
        this.refresco = refresco;
        this.extraQueso = extraQueso;
        this.salsa = salsa;
    }

    public Bocadillo(String _id, String nombre, String imagen, String[] ingredientes, float precio, float descuento, String bocateria, boolean refresco, boolean extraQueso, String salsa) {
        this._id = _id;
        this.nombre = nombre;
        this.imagen = imagen;
        this.ingredientes = ingredientes;
        this.precio = precio;
        this.descuento = descuento;
        this.bocateria = bocateria;
        this.refresco = refresco;
        this.extraQueso = extraQueso;
        this.salsa = salsa;
    }

    public Bocadillo() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String[] getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String[] ingredientes) {
        this.ingredientes = ingredientes;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getBocateria() {
        return bocateria;
    }

    public void setBocateria(String bocateria) {
        this.bocateria = bocateria;
    }

    public boolean isRefresco() {
        return refresco;
    }

    public void setRefresco(boolean refresco) {
        this.refresco = refresco;
    }

    public boolean isExtraQueso() {
        return extraQueso;
    }

    public void setExtraQueso(boolean extraQueso) {
        this.extraQueso = extraQueso;
    }

    public String getSalsa() {
        return salsa;
    }

    public void setSalsa(String salsa) {
        this.salsa = salsa;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public float getDescuento() {
        return descuento;
    }

    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bocadillo bocadillo = (Bocadillo) o;

        if (Float.compare(bocadillo.getPrecio(), getPrecio()) != 0) return false;
        if (isRefresco() != bocadillo.isRefresco()) return false;
        if (isExtraQueso() != bocadillo.isExtraQueso()) return false;
        if (getNombre() != null ? !getNombre().equals(bocadillo.getNombre()) : bocadillo.getNombre() != null)
            return false;
        if (getImagen() != null ? !getImagen().equals(bocadillo.getImagen()) : bocadillo.getImagen() != null)
            return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getIngredientes(), bocadillo.getIngredientes())) return false;
        if (getBocateria() != null ? !getBocateria().equals(bocadillo.getBocateria()) : bocadillo.getBocateria() != null)
            return false;
        return getSalsa() != null ? getSalsa().equals(bocadillo.getSalsa()) : bocadillo.getSalsa() == null;
    }

    @Override
    public int hashCode() {
        int result = getNombre() != null ? getNombre().hashCode() : 0;
        result = 31 * result + (getImagen() != null ? getImagen().hashCode() : 0);
        result = 31 * result + Arrays.hashCode(getIngredientes());
        result = 31 * result + (getPrecio() != +0.0f ? Float.floatToIntBits(getPrecio()) : 0);
        result = 31 * result + (getBocateria() != null ? getBocateria().hashCode() : 0);
        result = 31 * result + (isRefresco() ? 1 : 0);
        result = 31 * result + (isExtraQueso() ? 1 : 0);
        result = 31 * result + (getSalsa() != null ? getSalsa().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Bocadillo{" +
                "_id='" + _id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", imagen='" + imagen + '\'' +
                ", ingredientes=" + Arrays.toString(ingredientes) +
                ", precio=" + precio +
                ", descuento=" + descuento +
                ", bocateria='" + bocateria + '\'' +
                ", refresco=" + refresco +
                ", extraQueso=" + extraQueso +
                ", salsa='" + salsa + '\'' +
                '}';
    }
}
