package com.averano.bocatapp.mainActivity;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.averano.bocatapp.R;
import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.models.Bocadillo;
import com.averano.bocatapp.models.Pedido;
import com.averano.bocatapp.models.PedidoHist;

import org.parceler.Parcels;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BocadillosFavoritosFragment.OnListFragmentInteractionListener,
        PedidoHistorialFragment.OnListFragmentInteractionListener{

    TextView nUsuario, correo;
    String token;
    AuthUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = Parcels.unwrap(getIntent().getParcelableExtra("user"));
        token = user.getToken();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(user.getNombre());
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.setCheckedItem(R.id.nav_map);

        Fragment f = new MapaFragment();
        Bundle bundle = new Bundle();
        Parcelable parcel = Parcels.wrap(user);
        bundle.putParcelable("user", parcel);
        f.setArguments(bundle);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.mainFrame, f);
        ft.commit();

        nUsuario = navigationView.getHeaderView(0).findViewById(R.id.nombreUsuario);
        correo = navigationView.getHeaderView(0).findViewById(R.id.correoUsuario);

        nUsuario.setText(user.getNombre());
        correo.setText(user.getEmail());

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        Fragment f = null;

        if (id == R.id.nav_map) {
            f = new MapaFragment();
            Bundle bundle = new Bundle();
            Parcelable parcel = Parcels.wrap(user);
            bundle.putParcelable("user", parcel);
            f.setArguments(bundle);
        }else if (id == R.id.fav){
            f = new BocadillosFavoritosFragment();
            Bundle bundle = new Bundle();
            Parcelable parcel = Parcels.wrap(user);
            bundle.putParcelable("user", parcel);
            f.setArguments(bundle);
        }else if (id == R.id.historial){
            f = new PedidoHistorialFragment();
            Bundle bundle = new Bundle();
            Parcelable parcel = Parcels.wrap(user);
            bundle.putParcelable("user", parcel);
            f.setArguments(bundle);
        }

        if (f != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.mainFrame, f);
            ft.commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onListFragmentInteraction(Bocadillo item) {

    }

    @Override
    public void onListFragmentInteraction(PedidoHist item) {

    }
}
