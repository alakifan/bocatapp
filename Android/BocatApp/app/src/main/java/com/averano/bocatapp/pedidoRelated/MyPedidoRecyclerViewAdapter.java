package com.averano.bocatapp.pedidoRelated;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.averano.bocatapp.R;
import com.averano.bocatapp.models.Bocadillo;
import com.averano.bocatapp.services.PrecioChanger;

import java.util.ArrayList;
import java.util.List;

public class MyPedidoRecyclerViewAdapter extends RecyclerView.Adapter<MyPedidoRecyclerViewAdapter.ViewHolder> {

    private final List<Bocadillo> mValues;
    private Context ctx;
    private List<String> salsaList = new ArrayList<>();
    private PrecioChanger precioChanger;

    public MyPedidoRecyclerViewAdapter(Context context, List<Bocadillo> items) {
        mValues = items;
        ctx = context;

        salsaList.add(ctx.getString(R.string.ninguna));
        salsaList.add("mojo picón");
        salsaList.add("ali oli");
        salsaList.add(ctx.getString(R.string.mayonesa));
        salsaList.add("ketchup");

        precioChanger = (PrecioChanger) ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_pedido, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        final float[] precio = {mValues.get(position).getPrecio()};

        holder.nombre.setText(mValues.get(position).getNombre());
        holder.refresco.setChecked(mValues.get(position).isRefresco());
        if (holder.refresco.isChecked()){
            precio[0] = precio[0] + 0.8f;
            precioChanger.changePrecio(0.8f);
        }
        holder.refresco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.refresco.isChecked()){
                    precio[0] = precio[0] + 0.8f;
                    holder.precio.setText(String.format("%.2f", precio[0]) + "€");
                    precioChanger.changePrecio(0.8f);
                } else{
                    precio[0] = precio[0] - 0.8f;
                    holder.precio.setText(String.format("%.2f", precio[0]) + "€");
                    precioChanger.changePrecio(-0.8f);
                }
            }
        });
        holder.extraQueso.setChecked(mValues.get(position).isExtraQueso());
        if (holder.extraQueso.isChecked()){
            precio[0] = precio[0] + 0.5f;
            precioChanger.changePrecio(0.5f);
        }
        holder.extraQueso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.extraQueso.isChecked()){
                    precio[0] = precio[0] + 0.5f;
                    holder.precio.setText(String.format("%.2f", precio[0]) + "€");
                    precioChanger.changePrecio(0.5f);
                } else{
                    precio[0] = precio[0] - 0.5f;
                    holder.precio.setText(String.format("%.2f", precio[0]) + "€");
                    precioChanger.changePrecio(-0.5f);
                }
            }
        });
        holder.precio.setText(String.format("%.2f", precio[0]) + "€");
        if (mValues.get(position).getSalsa().equals(ctx.getString(R.string.ninguna)))
            holder.spinnerSalsa.setSelection(0);
        else if (mValues.get(position).getSalsa().equals("mojo picón"))
            holder.spinnerSalsa.setSelection(1);
        else if (mValues.get(position).getSalsa().equals("ali oli"))
            holder.spinnerSalsa.setSelection(2);
        else if (mValues.get(position).getSalsa().equals(ctx.getString(R.string.mayonesa)))
            holder.spinnerSalsa.setSelection(3);
        else if (mValues.get(position).getSalsa().equals("ketchup"))
            holder.spinnerSalsa.setSelection(4);
    }

    @Override
    public int getItemCount() {
        if (mValues == null){
            return 0;
        }
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView nombre;
        public final Spinner spinnerSalsa;
        public final CheckBox refresco;
        public final CheckBox extraQueso;
        public final TextView precio;
        public final ImageView cancelar;
        public Bocadillo mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nombre = view.findViewById(R.id.nombreBocataPedido);
            spinnerSalsa = view.findViewById(R.id.listaSalsas);
            spinnerSalsa.setAdapter(new ArrayAdapter<>(ctx, android.R.layout.simple_spinner_dropdown_item, salsaList));
            refresco = view.findViewById(R.id.refresco);
            extraQueso = view.findViewById(R.id.extraQueso);
            precio = view.findViewById(R.id.precioItem);
            cancelar = view.findViewById(R.id.botonCancelar);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }
}
