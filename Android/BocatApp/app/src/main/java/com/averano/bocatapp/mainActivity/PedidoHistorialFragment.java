package com.averano.bocatapp.mainActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.averano.bocatapp.R;
import com.averano.bocatapp.models.AuthUser;
import com.averano.bocatapp.models.PedidoHist;
import com.averano.bocatapp.services.BocatAppAPI;
import com.averano.bocatapp.services.ServiceGenerator;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PedidoHistorialFragment extends Fragment {

    private int mColumnCount = 1;
    AuthUser user;
    List<PedidoHist> pedidos;
    ProgressDialog nDialog;

    private OnListFragmentInteractionListener mListener;

    public PedidoHistorialFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        nDialog = new ProgressDialog(getContext());
        nDialog.setMessage(getString(R.string.cargando));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_pedido_historial_list, container, false);

        user = Parcels.unwrap(getArguments().getParcelable("user"));
        pedidos = new ArrayList<>();

        nDialog.show();

        BocatAppAPI api = ServiceGenerator.createService(BocatAppAPI.class);

        Call<List<PedidoHist>> call = api.getPedidos("Bearer " + user.getToken(), user.get_id());

        call.enqueue(new Callback<List<PedidoHist>>() {
            @Override
            public void onResponse(Call<List<PedidoHist>> call, Response<List<PedidoHist>> response) {
                if (response.isSuccessful()){
                    pedidos = response.body();

                    if (view instanceof RecyclerView) {
                        Context context = view.getContext();
                        RecyclerView recyclerView = (RecyclerView) view;
                        if (mColumnCount <= 1) {
                            recyclerView.setLayoutManager(new LinearLayoutManager(context));
                        } else {
                            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
                        }
                        recyclerView.setAdapter(new MyPedidoHistorialRecyclerViewAdapter(getContext(), pedidos, user));
                    }
                    nDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<PedidoHist>> call, Throwable t) {
                t.printStackTrace();
            }
        });
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(PedidoHist item);
    }
}
